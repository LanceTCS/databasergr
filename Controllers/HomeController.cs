﻿using DatabaseRGR.Models;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using System.Diagnostics;
using System.Globalization;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace DatabaseRGR.Controllers {
    public class HomeController : Controller {
        private static Database db = new Database();
        private static string ConnectionString = "";
        public static bool IsAuthorized = false;
        public static User user;
        private static string Message = "";
        public IActionResult Index() {
            try {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                if (!IsAuthorized) {
                    return View("Login");
                }
                return View();
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnLogin(string login, string password) {
            try {
                db.Login(login, password);
                return RedirectToAction("Index");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult Register() {
            try {
                ViewData["IsAuthorized"] = IsAuthorized;
                return View("Register");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnRegister(string login, string password) {
            try {
                db.Register(login, password);
                Message = "Successfully registered";
                return RedirectToAction("Index");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult AddAction() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.ActionTypes = db.SelectActionsTypes();
                return View("AddAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnAddAction(string actionType, string calories, string duration, string date) {
            try {
                db.AddAction(actionType, calories, duration, date);
                Message = "Successfully added";
                return RedirectToAction("AddAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult EditAction() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.ActionTypes = db.SelectActionsTypes();
                ViewBag.Actions = db.SelectActions();
                ViewBag.ActionIds = db.SelectActions().Select(o => o.Id).ToList();
                return View("EditAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnEditAction(string id, string actionType, string calories, string duration, string date) {
            try {
                db.EditAction(id, actionType, calories, duration, date);
                Message = "Successfully edited";
                return RedirectToAction("EditAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult DeleteAction() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.Actions = db.SelectActions();
                ViewBag.ActionIds = db.SelectActions().Select(o => o.Id).ToList();
                return View("DeleteAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnDeleteAction(string id) {
            try {
                db.DeleteAction(id);
                Message = "Successfuly deleted";
                return RedirectToAction("DeleteAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult ShowActions() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.ActionTypes = db.SelectActionsTypes();
                ViewBag.Actions = db.SelectActions();
                ViewBag.ActionIds = db.SelectActions().Select(o => o.Id).ToList();
                return View("ShowActions");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnShowActions(string actionType, string calories, string duration, string date) {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.ActionTypes = db.SelectActionsTypes();
                ViewBag.Actions = db.SelectFilteredActions(actionType, calories, duration, date);
                return View("ShowActions");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult AddStat() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                return View("AddStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnAddStat(string weight, string fat, string muscle, string date) {
            try {
                db.AddStat(weight, fat, muscle, date);
                Message = "Successfuly added";
                return RedirectToAction("AddStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult EditStat() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.Stats = db.SelectStats();
                return View("EditStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnEditStat(string id, string weight, string fat, string muscle, string date) {
            try {
                db.EditStat(id, weight, fat, muscle, date);
                Message = "Successfuly edited";
                return RedirectToAction("EditStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult DeleteStat() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.Stats = db.SelectStats();
                return View("DeleteStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnDeleteStat(string id) {
            try {
                db.DeleteStat(id);
                Message = "Successfuly deleted";
                return RedirectToAction("DeleteStat");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult ShowStats() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.Stats = db.SelectStats();
                return View("ShowStats");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult OnShowStats(string weight, string fat, string muscle, string date) {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewBag.Stats = db.SelectFilteredStats(weight, fat, muscle, date);
                return View("ShowStats");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult GenerateStats() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                db.GenerateStats();
                return View("GenerateStats");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult GenerateAction() {
            try {
                ViewData["Message"] = Message;
                Message = "";
                ViewData["IsAuthorized"] = IsAuthorized;
                db.GenerateAction();
                return View("GenerateAction");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
        public IActionResult Logout() {
            try {
                IsAuthorized = false;
                return RedirectToAction("Index");
            }
            catch {
                ViewData["IsAuthorized"] = IsAuthorized;
                ViewData["Message"] = Message;
                Message = "";
                return View("ErrorPage");
            }
        }
    }
}