﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseRGR.Models {
    public class Action {
        public int Id { get; set; }
        public int? IdActionType { get; set; }
        [ForeignKey("IdActionType")]
        public ActionType ActionType { get; set; }
        public int? IdUser { get; set; }
        [ForeignKey("IdUser")]
        public User User { get; set; }
        public int CaloriesBurnt { get; set; }
        public TimeOnly Duration { get; set; }
        public DateOnly DateDone { get; set; }
        public Action() { }
        public Action(int id, ActionType actionType, User user, int burntCalories, TimeOnly duration, DateOnly dateDone) {
            Id = id;
            ActionType = actionType;
            User = user;
            CaloriesBurnt = burntCalories;
            Duration = duration;
            DateDone = dateDone;
        }
    }
}
