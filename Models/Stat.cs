﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseRGR.Models {
    public class Stat {
        public int Id { get; set; }
        public int? IdUser { get; set; }
        [ForeignKey("IdUser")]
        public User User { get; set; }
        public double Weight { get; set; }
        public double FatPercent { get; set; }
        public double MusclePercent { get; set; }
        public DateOnly DateDone { get;set; }
        public Stat() { }
        public Stat(int id, User user, double weight, double fatPercent, double musclePercent, DateOnly dateDone) {
            Id = id;
            User = user;
            Weight = weight;
            FatPercent = fatPercent;
            MusclePercent = musclePercent;
            DateDone = dateDone;
        }
    }
}
