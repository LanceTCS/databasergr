﻿using DatabaseRGR.Controllers;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Collections.ObjectModel;

namespace DatabaseRGR.Models {
    public class Database : DbContext {
        private static string ConnectionString = "Server=localhost;Port=5432;User ID=postgres;Password=123;Database=FitnessTracker;";
        public DbSet<User> Users { get; set; }
        public DbSet<Action> Actions { get; set; }
        public DbSet<Stat> OverallStats { get; set; }
        public DbSet<ActionType> ActionTypes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Action>().Navigation(o => o.ActionType).AutoInclude();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseNpgsql(ConnectionString);
        }
        
        public void Login(string login, string password) {
            User u = Users.SingleOrDefault(o => o.Username == login && o.Password == password);
            if(u != null) {
                HomeController.IsAuthorized = true;
                HomeController.user = u;
            }
        }
        public void Register(string login, string password) {
            User u = new User() { Username = login, Password = password };
            Users.Add(u);
            SaveChanges();
        }
        public List<string> SelectActionsTypes() {
            return ActionTypes.Select(o => o.Name).ToList();
        }
        public void AddAction(string actionType, string calories, string duration, string date) {
            DateTime td = Convert.ToDateTime(date);
            DateTime to = Convert.ToDateTime(duration);
            ActionType at = ActionTypes.Single(o => o.Name == actionType);
            Action a = new Action() {
                ActionType = at,
                CaloriesBurnt = Convert.ToInt32(calories),
                User = HomeController.user,
                DateDone = new DateOnly(td.Year, td.Month, td.Day),
                Duration = new TimeOnly(to.Hour, to.Minute, to.Second)
            };
            Actions.Add(a);
            SaveChanges();
        }
        public List<Models.Action> SelectActions() {
            return Actions.ToList();
        }
        public void EditAction(string id, string actionType, string calories, string duration, string date) {
            DateTime td = Convert.ToDateTime(date);
            DateTime to = Convert.ToDateTime(duration);
            ActionType at = ActionTypes.Single(o => o.Name == actionType);
            Action a = Actions.Single(o => o.Id.ToString() == id);
            a.ActionType = at;
            a.CaloriesBurnt = Convert.ToInt32(calories);
            a.DateDone = new DateOnly(td.Year, td.Month, td.Day);
            a.Duration = new TimeOnly(to.Hour, to.Minute, to.Second);
            SaveChanges();
        }
        public void DeleteAction(string id) {
            Action a = Actions.Single(o => o.Id.ToString() == id);
            Actions.Remove(a);
            SaveChanges();
        }
        public List<Models.Action> SelectFilteredActions(string actionType, string calories, string duration, string date) {
            if(actionType == "All") {
                return Actions.Where(o => o.CaloriesBurnt.ToString() == calories && o.Duration.ToString() == duration && o.DateDone.ToShortDateString() == date).ToList();
            }
            else {
                return Actions.Where(o => o.ActionType.Name == actionType && o.CaloriesBurnt.ToString() == calories && o.Duration.ToString() == duration && o.DateDone.ToShortDateString() == date).ToList();
            }
        }
        public void AddStat(string weight, string fat, string muscle, string date) {
            DateTime td = Convert.ToDateTime(date);
            Stat s = new Stat() {
                Weight = Convert.ToDouble(weight),
                FatPercent = Convert.ToDouble(fat),
                MusclePercent = Convert.ToDouble(muscle),
                DateDone = new DateOnly(td.Year, td.Month, td.Day),
                User = HomeController.user
            };
            OverallStats.Add(s);
            SaveChanges();
        }
        public List<Stat> SelectStats() {
            return OverallStats.ToList();
        }
        public void EditStat(string id, string weight, string fat, string muscle, string date) {
            DateTime td = Convert.ToDateTime(date);
            Stat s = OverallStats.Single(o => o.Id.ToString() == id);
            s.Weight = Convert.ToDouble(weight);
            s.FatPercent = Convert.ToDouble(fat);
            s.MusclePercent = Convert.ToDouble(muscle);
            s.DateDone = new DateOnly(td.Year, td.Month, td.Day);
            SaveChanges();
        }
        public void DeleteStat(string id) {
            Stat s = OverallStats.Single(o => o.Id.ToString() == id);
            OverallStats.Remove(s);
            SaveChanges();
        }
        public List<Stat> SelectFilteredStats(string weight, string fat, string muscle, string date) {
            DateTime td = Convert.ToDateTime(date);
            return OverallStats.Where(o => o.Weight.ToString() == weight && o.FatPercent.ToString() == fat &&
            o.MusclePercent.ToString() == muscle && o.DateDone.ToShortDateString() == td.ToShortDateString()).ToList();
        }
        public void GenerateStats() {
            string weight = "";
            string fat = "";
            string muscle = "";
            string date = "";
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT random() * 60 + 40 AS RAND_1_11", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    weight = reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT random() * 20 + 5 AS RAND_1_11", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    fat = reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT random() * 30 + 40 AS RAND_1_11", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    muscle = reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 4 + 2020)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString() + "-";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 12 + 1)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString() + "-";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 28 + 1)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"INSERT INTO \"OverallOverallStats\"(\"IdUser\", \"Weight\", \"FatPercent\", \"MusclePercent\", \"DateDone\") VALUES({HomeController.user.Id}, {weight}, {fat}, {muscle}, '{date}')", connection);
                command.ExecuteNonQuery();
            }
        }
        public void GenerateAction() {
            string actionType = "";
            string calories = "";
            string duration = "";
            string date = "";
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 3 + 0)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    actionType = reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 500 + 100)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    calories = reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 24 + 0)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    duration += reader.GetValue(0).ToString() + ":";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 59 + 0)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    duration += reader.GetValue(0).ToString() + ":";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 59 + 0)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    duration += reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 4 + 2020)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString() + "-";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 12 + 1)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString() + "-";
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"SELECT Floor(random() * 28 + 1)::int", connection);
                NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    reader.Read();
                    date += reader.GetValue(0).ToString();
                }
            }
            using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString)) {
                connection.Open();
                NpgsqlCommand command = new NpgsqlCommand($"Insert into \"Actions\"(\"IdActionType\", \"IdUser\", \"CaloriesBurnt\", \"Duration\", \"DateDone\") Values({actionType}, {HomeController.user.Id}, {calories}, '{duration}', '{date}')", connection);
                command.ExecuteNonQuery();
            }
        }
    }
}
